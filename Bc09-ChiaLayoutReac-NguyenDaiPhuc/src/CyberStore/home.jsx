import React, { Component } from "react";
import Footer from "./footer";
import Header from "./header";
import PLaptop from "./pLaptop";
import PSmartPhone from "./pSmartPhone";
import Slider from "./slider";

class Home extends Component {
  render() {
    return (
      <div style={{ backgroundColor: "#708090" }}>
        <Header />
        <Slider />
        <PSmartPhone />
        <PLaptop />
        <Footer />
      </div>
    );
  }
}

export default Home;
