import logo from "./logo.svg";
import "./App.css";
import Home from "./CyberStore/home";

function App() {
  return (
    <div className="App">
      <Home />
    </div>
  );
}

export default App;
